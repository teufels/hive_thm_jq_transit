###################
## SETUP TRANSIT ##
###################

[globalVar = LIT:1 = {$plugin.tx_hive_thm_jq_transit.settings.production.optional.active}] && [globalVar = LIT:1 > {$plugin.tx_hive_cfg_typoscript.settings.gulp}]
    page {
        includeJSFooterlibs {
            useTransit = {$plugin.tx_hive_thm_jq_transit.settings.production.includePath.public}Assets/Js/use_transit.min.js
            useTransit.async = 1
        }
    }
[global]